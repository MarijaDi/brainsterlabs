<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="style.css">
    <title>Вработи студент</title>
</head>

<body>
    <nav class="nav navbar-default yellowNavvraboti borderRadius">
        <div class="container-fluid">
            <div class="col-md-1 heightTabletLogo">
                <div class="navbar-header logoTablet">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                </button>
                    <a class="navbar-brand" href="index.php">
                        <img src="brainsterlogo.png" alt="brainster logo" id="logoBrainster">
                    </a>
                </div>
            </div>
            <div class="col-md-11">
                <div class="collapse navbar-collapse navigationBar" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="https://www.brainster.io/marketpreneurs" target="_blank" class="navBlack">Академија за маркетинг</a></li>
                        <li><a href="http://codepreneurs.co/" target="_blank" class="navBlack">Академија за програмирање</a></li>
                        <li><a href="https://www.brainster.io/data-science" target="_blank" class="navBlack">Академија за data science</a></li>
                        <li><a href="https://www.brainster.io/design" target="_blank" class="navBlack navTablet">Академија за дизајн</a></li>
                        <li class="navigationButton"><a class="btn redButton navTablet" href="vrabotistudent.php" role="button">Вработи наш студент</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid vrabotiYellow">
    <div class="row"> 
        <div class="navigation">
        <a class="navbar-brand" href="index.php">
                        <img src="brainsterlogo.png" alt="brainster logo" id="logoBrainster">
                    </a>
                <div onclick="showSidebar()" class="toggle-nav">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <div class="sidebar">
                    <a onclick="closeSidebar()"><i class="fa fa-times"></i></a>
                    <a href="https://www.brainster.io/marketpreneurs"  target="_blank">Академија за маркетинг</a>
                    <a href="http://codepreneurs.co/"  target="_blank">Академија за програмирање</a>
                    <a href="https://www.brainster.io/data-science"  target="_blank">Академија за data science</a>
                    <a href="https://www.brainster.io/design"  target="_blank">Академија за дизајн</a>
                    <a href="vrabotistudent.php" class="vrabotiMobile">Вработи наш студент</a>

                </div>
        </div></div>    
        <div class="row text-center">
            <p class="vrabotiText">Вработи студенти</p>
        </div>
        <form method="post" action="success.php">
        <div class="row width70">
            <div class="col-md-6 col-sm-6 paddingMobileForm6"><label for="firstNameLastName" class="labelForm">Име и презиме</label>
                <br>
                <input type="text" name="firstNameLastName" id="firstNameLastName" class="inputForm" placeholder="Вашето име и презиме" required>
            </div>
            <div class="col-md-6 col-sm-6 paddingMobileForm">
                <label for="companyName" class="labelForm">Име на компанија</label>
                <br>
                <input type="text" name="companyName" id="companyName" class="inputForm" placeholder="Името на вашата компанија" required>
            </div>
        </div>
        <div class="row width70 paddingTop">
            <div class="col-md-6 col-sm-6 paddingMobileForm">
                <label for="email" class="labelForm">Контакт имејл</label>
                <br>
                <input type="email" name="email" id="email" class="inputForm" placeholder="Контакт имејл на вашата компанија" required>
            </div>
            <div class="col-md-6 col-sm-6 paddingMobileForm">
                <label for="phoneNumber" class="labelForm">Контакт телефон</label>
                <br>
                <input type="number" name="phoneNumber" id="phoneNumber" class="inputForm" placeholder="Контакт телефон на вашата компанија" required>
            </div>
        </div>
        <div class="row width70 paddingTop">
            <div class="col-md-6 col-sm-6 paddingMobileForm">
            <label for="typeOfStudent" class="labelForm">Тип на студент</label>
                <select name="typeOfStudent" id="typeOfStudent" class="select" required>
                <option value="" class="optionSelect">Избери тип на студенти</option>
                <option value="marketing" class="optionSelect">Студенти на маркетинг</option>
                <option value="it" class="optionSelect">Студенти на програмирање</option>
                <option value="data-science" class="optionSelect">Студенти на data science</option>
                <option value="design" class="optionSelect">Студенти на дизајн</option>
                </select>
            </div>
            <div class="col-md-6 col-sm-6 paddingTop paddingMobileFormButton">
                <button type="sumbit" class="btn buttonIsprati text-uppercase" name="send">Испрати</button>
            </div>
        </div>
        </form> 
        <div class="row paddingSpace"></div>
        <div class="row gray">
            <div class="col-md-12 text-center textFooter">
                Изработено од Марија Димеска, студент на Brainster
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>

</html>


<?php

$username = 'root';
$password = 'root';
$database = 'employers';
$conn = new mysqli('localhost', $username, $password, $database) 

or 

die("Connect failed: %s\n". $conn -> error);

    $firstNameLastName = 'empty';
    $companyName ='empty';
    $email ='empty';
    $phoneNumber = 'empty';
    $typeOfStudent = 'empty';
 
if (isset($_POST ['send'])) {
    $firstNameLastName = $_POST['firstNameLastName'];
    $companyName = $_POST['companyName'];
    $email = $_POST['email'];
    $phoneNumber = $_POST['phoneNumber'];
    $typeOfStudent = $_POST['typeOfStudent'];
}

$sql = "INSERT INTO employers (firstNameLastName,companyName,email,phoneNumber,typeOfStudent) VALUES ('$firstNameLastName', '$companyName', '$email', '$phoneNumber','$typeOfStudent')";


?>

<script>

function showSidebar() {
    let mySidebar = document.querySelector('.sidebar');
    mySidebar.style.width = '100%';
}

function closeSidebar() {
    let mySidebar = document.querySelector('.sidebar');
    mySidebar.style.width = '0';
}

</script>